package ru.dshalamov.test

import java.net.MalformedURLException
import java.nio.channels.OverlappingFileLockException
import java.util.concurrent.atomic.AtomicInteger

import ru.dshalamov.test.util._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.swing._
import scala.swing.event.ButtonClicked
import scala.util.{Failure, Success}

/**
  * The simple application thad download files from remote source.
  * Supported protocols: http, https, ftp, ftps, sftp
  */
object Main extends SimpleSwingApplication {

  // target directory to save files
  private var directory: String = _

  private val activeDownloadCounts = new AtomicInteger(0)

  implicit val remoteClient: RemoteClient = new RemoteClient

  /** ***************  GUI  ***************************/

  // container for progress bars
  private val progressPanel = new BoxPanel(Orientation.Vertical)

  private val downloadBtn = new Button("Download") {
    visible = false
  }

  private val urlsTextArea = new TextArea {
    rows = 8
    columns = 40
  }

  private val directoryChooserBtn = new Button("Choose directory")


  /**
    * Configure frame
    *
    * @return
    */
  override def top: Frame = new MainFrame() {
    title = "Home Test"
    visible = true

    contents = new BoxPanel(Orientation.Vertical) {
      border = Swing.EmptyBorder(10, 10, 10, 10)
      contents += progressPanel
      contents += Swing.VStrut(10)
      contents += Swing.Glue

      contents += urlsTextArea

      contents += Swing.VStrut(10)
      contents += Swing.Glue
      contents += new BoxPanel(Orientation.Horizontal) {
        contents += directoryChooserBtn
        contents += downloadBtn
      }
    }

    listenTo(downloadBtn)
    listenTo(directoryChooserBtn)

    reactions += {
      case ButtonClicked(`downloadBtn`) => onDownloadBtnClick()
      case ButtonClicked(`directoryChooserBtn`) => onDirectoryChooserBtnClick()
    }
  }

  /** *****************  End of Gui *********************/

  /** *********************  Event handlers  ********************/

  /**
    * Validate directory and enable dowbloading
    */
  private def onDirectoryChooserBtnClick(): Unit = {
    val chooser = new FileChooser() {
      fileSelectionMode = FileChooser.SelectionMode.DirectoriesOnly
    }

    if (FileChooser.Result.Approve == chooser.showOpenDialog(null)) {
      val file = chooser.selectedFile

      if (file.canWrite) {
        directory = file.getPath

        Swing.onEDT {
          directoryChooserBtn.visible = false
          downloadBtn.visible = true
        }
      }
    }
  }

  /**
    * Explode urls by lines and start downloading
    */
  private def onDownloadBtnClick(): Unit = {
    if (null != directory) {
      urlsTextArea.text.split("\\n").map(_.trim) foreach retrieveFile
    }
  }

  /**
    * Decrement counter of active downloads and try close application
    */
  private def onDownloadSuccess(): Unit = decrementCounterAndExit()

  /**
    * Show errors
    *
    * @param panel - the progress bar container
    * @param msg   - error message
    */
  private def onDownloadFail(panel: BoxPanel, msg: String): Unit = {
    decrementCounterAndExit()

    Swing.onEDT {
      panel.contents.remove(2)
      panel.contents += new Label(msg)
      panel.revalidate()
      panel.repaint()
    }
  }

  /** *********************  End of Event handlers  ********************/


  /**
    * @param url - Remote file url
    */
  private def retrieveFile(url: String): Unit = {
    //create progress bar
    val progressBar = new ProgressBar {
      min = 0
      max = 100
    }

    val panel = new BoxPanel(Orientation.Vertical) {
      border = Swing.EmptyBorder(5, 0, 10, 0)

      contents += new Label(url)
      contents += Swing.VStrut(3)
      contents += progressBar
    }

    Swing.onEDT {
      progressPanel.contents += panel
      progressPanel.revalidate()
      progressPanel.repaint()
    }

    Future {
      activeDownloadCounts.incrementAndGet()

      Retriever.retrieveFile(url, directory) { (size, total) =>
        progressBar.value = (size * 100 / total).toInt
      }
    } onComplete {
      case Success(_) => onDownloadSuccess()
      case Failure(_: OverlappingFileLockException) => onDownloadFail(panel, "Already started")
      case Failure(_: MalformedURLException) => onDownloadFail(panel, "Invalid URL")
      case Failure(_: Throwable) => onDownloadFail(panel, "Error")
    }
  }

  private def decrementCounterAndExit(): Unit = if (activeDownloadCounts.decrementAndGet() == 0) {
    sys.exit()
  }
}
