package ru.dshalamov.test.net

import java.io.{IOException, InputStream}
import java.net.{ConnectException, HttpURLConnection}

object HttpRemoteFile {
  /**
    * Create new instance of HttpRemoteFile
    *
    * @param url - URL instance
    * @return
    */
  def apply(url: URL): HttpRemoteFile = new HttpRemoteFile(url)
}

/**
  * Http/Https remote file
  *
  * @param url - URL instance
  */
@throws(classOf[IOException])
class HttpRemoteFile(url: URL) extends RemoteFile {
  private var connection: HttpURLConnection = _

  private var fileName: String = _

  private var fileSize: Long = 0

  /**
    * @inheritdoc
    */
  def connect(): Unit = {
    close()
    connection = url.toBaseURL.openConnection.asInstanceOf[HttpURLConnection]

    if (fileName == null) {
      val info = connection.getHeaderField("Content-Disposition")
      fileName =
        if (info != null) {
          "filename\\*=.*''(.+)$".r.findFirstMatchIn(info) match {
            case Some(matches) => matches.group(1)
            case None => url.file.substring(url.file.lastIndexOf("/") + 1)
          }
        } else {
          url.file.substring(url.file.lastIndexOf("/") + 1)
        }
    }

    if (fileSize == 0) {
      fileSize = connection.getContentLength
    }
  }

  /**
    * @inheritdoc
    */
  def getFileSize: Long = fileSize

  /**
    * @inheritdoc
    */
  @throws(classOf[IOException])
  def getInputStream(skip: Long): InputStream = {
    if (connection == null) {
      throw new ConnectException(s"Connection is not created")
    }

    try {
      if (skip > 0) {
        connection.setRequestProperty("Range", "bytes=" + skip + "-")
      }

      connection.getResponseCode match {
        case 206 => connection.getInputStream
        case _ =>
          val stream = connection.getInputStream
          stream.skip(skip)
          stream
      }
    } catch {
      case e: IllegalStateException =>
        connect()
        getInputStream(skip)
    }
  }

  /**
    * @inheritdoc
    */
  def getFileName: String = fileName

  /**
    * @inheritdoc
    */
  override def close(): Unit = {
    try {
      if (connection != null) {
        connection.disconnect()
      }
    } catch {
      case e: Throwable =>
    }
  }
}
