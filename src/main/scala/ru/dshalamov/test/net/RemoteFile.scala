package ru.dshalamov.test.net

import java.io.{IOException, InputStream}

/**
  * Interface of remote file
  */
trait RemoteFile extends AutoCloseable {
  /**
    * File size
    *
    * @return
    */
  def getFileSize: Long

  /**
    *
    * @param skip - first byte
    * @throws java.io.IOException If an I/O error occurs while either sending a
    *                             command to the server or receiving a reply from the server
    * @return
    */
  @throws(classOf[IOException])
  def getInputStream(skip: Long): InputStream

  /**
    * File name
    *
    * @return
    */
  def getFileName: String

  /**
    * Open connection
    */
  @throws(classOf[IOException])
  def connect(): Unit

  /**
    * Close active connection
    */
  def close(): Unit
}
