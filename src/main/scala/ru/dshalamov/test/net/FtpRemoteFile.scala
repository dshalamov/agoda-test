package ru.dshalamov.test.net

import java.io.{FileNotFoundException, IOException, InputStream}

import org.apache.commons.net.ftp.{FTP, FTPClient}

object FtpRemoteFile {
  /**
    * Create new instance of FtpRemoteFile
    *
    * @param url - URL instance
    * @return
    */
  def apply(url: URL): FtpRemoteFile = {
    new FtpRemoteFile(new FTPClient(), url)
  }
}

/**
  * ftp/ftps remote file
  *
  * @param client - FTPClient instance
  * @param url    - URL instance
  */
@throws(classOf[IOException])
class FtpRemoteFile(client: FTPClient, url: URL) extends RemoteFile {

  private var fileName: String = _

  private var fileSize: Long = _

  /**
    * @inheritdoc
    */
  @throws(classOf[IOException])
  override def connect(): Unit = {
    close()
    //connect to server
    url.port match {
      case None => client.connect(url.host)
      case Some(p) => client.connect(url.host, p)
    }

    //authenticate user
    val auth = (url.username, url.password) match {
      case (None, None) => client.login("anonymous", null)
      case (None, Some(p)) => client.login("anonymous", p)
      case (Some(u), None) => client.login(u, null)
      case (Some(u), Some(p)) => client.login(u, p)
    }

    if (!auth) {
      throw new IOException("Authentication failure")
    }

    client.setFileType(FTP.BINARY_FILE_TYPE)

    if (fileName == null || fileSize == 0) {

      val list = client.listFiles(url.file)

      if (list.isEmpty) {
        throw new FileNotFoundException(s"File ${url.file} not found")
      }

      if (list.length > 1) {
        throw new FileNotFoundException(s"'${url.file}' is a directory")
      }

      val file = list(0)

      fileName = file.getName
      fileSize = file.getSize
    }
  }


  /**
    * @inheritdoc
    */
  def getFileSize: Long = fileSize

  /**
    * @inheritdoc
    */
  def getFileName: String = fileName

  /**
    * @inheritdoc
    */
  @throws(classOf[IOException])
  def getInputStream(skip: Long): InputStream = {
    client.setRestartOffset(skip)
    client.retrieveFileStream(url.file) match {
      case x: InputStream => x
      //skip bytes on stream if server does not support partial content
      case _ =>
        val stream = client.retrieveFileStream(url.file)
        stream.skip(skip)
        stream
    }
  }

  /**
    * @inheritdoc
    */
  override def close(): Unit = {
    try {
      if (client.isConnected) {
        client.disconnect()
      }
    } catch {
      case _: Throwable =>
    }
  }
}
