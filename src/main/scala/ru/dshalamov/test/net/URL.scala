package ru.dshalamov.test.net

import java.io.IOException
import java.net.MalformedURLException

object URL {
  @throws(classOf[IOException])
  def apply(url: String): URL = new URL(url)
}

class URL(url: String) {
  private val regexp = "^(?<protocol>\\w+{3,5})://((?<username>[^:]+)(:(?<password>.*))?@)?(?<host>[^:/]+)(:(?<port>\\d+))?(?<file>/.*)?".r(
    "protocol",
    null,
    "username",
    null,
    "password",
    "host",
    null,
    "port",
    "file"
  )

  private var _protocol: String = _
  private var _username: String = _
  private var _password: String = _
  private var _host: String = _
  private var _port: String = _
  private var _file: String = _

  regexp.findFirstMatchIn(url) match {
    case Some(matches) =>
      _protocol = matches.group("protocol")
      _username = matches.group("username")
      _password = matches.group("password")
      _host = matches.group("host")
      _port = matches.group("port")
      _file = if (matches.group("file") != null) matches.group("file") else "/"
    case None => throw new MalformedURLException(s"Invalid url $url")
  }

  def protocol: String = _protocol

  def host: String = _host

  def file: String = _file

  def port: Option[Int] = if (_port == null) None else Some(_port.toInt)

  def username: Option[String] = Option(_username)

  def password: Option[String] = Option(_password)

  @throws(classOf[IOException])
  def toBaseURL: java.net.URL = new java.net.URL(url)
}
