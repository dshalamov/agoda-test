package ru.dshalamov.test.net

import java.io.{IOException, InputStream}
import java.net.ConnectException

import com.jcraft.jsch._

object SftpRemoteFile {
  /**
    * Create instance of SftpRemoteFile
    *
    * @param url - instance of URL
    * @return
    */
  def apply(url: URL): SftpRemoteFile = {
    new SftpRemoteFile(new JSch, url)
  }
}

/**
  * sftp remote file
  *
  * @param client - instance of JSch
  * @param url    - instance of URL
  */
class SftpRemoteFile(client: JSch, url: URL) extends RemoteFile {
  private var session: Session = _
  private var channel: ChannelSftp = _
  private var fileSize: Long = _

  private var connected = false


  /**
    * @inheritdoc
    */
  @throws(classOf[IOException])
  def connect(): Unit = {
    close()

    try {
      session = (url.username, url.port) match {
        case (Some(u), Some(p)) => client.getSession(u, url.host, p)
        case (Some(u), None) => client.getSession(u, url.host)
        case (None, Some(p)) => client.getSession(null, url.host, p)
        case (None, None) => client.getSession(url.host)
      }

      url.password match {
        case None =>
        case Some(p) => session.setPassword(p)
      }
      session.setConfig("StrictHostKeyChecking", "no")
      session.connect()

      channel = session.openChannel("sftp").asInstanceOf[ChannelSftp]
      channel.connect()

      fileSize = channel.stat(url.file).getSize

      connected = true

    } catch {
      //cast to IOException
      case e: JSchException =>
        close()
        throw new IOException(e.getMessage, e)
    }
  }

  /**
    * @inheritdoc
    */
  def getFileSize: Long = fileSize

  /**
    * @inheritdoc
    */
  @throws(classOf[IOException])
  def getInputStream(skip: Long): InputStream = {
    if (!connected) {
      throw new ConnectException(s"Connection is not created")
    }

    channel.get(url.file, null, skip)
  }

  /**
    * @inheritdoc
    */
  def getFileName: String = url.file.substring(url.file.lastIndexOf("/") + 1)

  /**
    * Close active connection
    */
  override def close(): Unit = {
    try {
      if (channel != null && channel.isConnected) {
        channel.disconnect()
      }
    } catch {
      case _: Throwable =>
    }

    try {
      if (session != null && session.isConnected) {
        session.disconnect()
      }
    } catch {
      case _: Throwable =>
    }

    connected = false
  }
}
