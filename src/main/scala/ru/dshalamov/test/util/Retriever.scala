package ru.dshalamov.test.util

import java.io._
import java.nio.channels.OverlappingFileLockException

object Retriever {

  /**
    * Retrieves a file from the remote server and save it to the given directory
    * Try to complete downloading if tmp file exists
    *
    * @param url       - Remote file URL
    * @param directory - Target directory
    * @param deleteTmp - Delete tmp file on error
    * @return
    */
  @throws(classOf[IOException])
  def retrieveFile(url: String, directory: String, deleteTmp: Boolean = true)
                  (onProgress: (Long, Long) => Unit)
                  (implicit remoteClient: RemoteClient): Long = {
    //create tmp file and lock
    val tmpFile = new File(directory + "/" + hash(url) + ".tmp")
    var fileSize = if (tmpFile.createNewFile()) 0 else tmpFile.length()
    val output = new FileOutputStream(tmpFile)

    try {
      output.getChannel.lock()

      try {
        val remoteFile = remoteClient.getRemoteFile(url)
        remoteFile.connect()
        val remoteFileName = remoteFile.getFileName
        val remoteFileSize = remoteFile.getFileSize
        val input = remoteFile.getInputStream(fileSize)

        try {
          copyBytes(input, output) { partSize =>
            fileSize += partSize
            onProgress(fileSize, remoteFileSize)
          }

          if (fileSize != remoteFileSize) {
            throw new SyncFailedException(s"File was downloaded incorrect")
          }

          tmpFile.renameTo(createNewFile(directory + "/" + remoteFileName))
        } finally {
          input.close()
          remoteFile.close()
        }
      } finally {
        output.close()
      }
    } catch {
      //do nothing if file already was locked by other process
      case e: OverlappingFileLockException => throw e
      case e: Throwable =>
        if (deleteTmp) {
          tmpFile.delete()
        }

        throw e
    }

    fileSize
  }

  private def copyBytes(input: InputStream, output: OutputStream)(onProgress: Long => Unit): Unit = {
    val buffer = new Array[Byte](1024)

    Iterator continually {
      input.read(buffer)
    } takeWhile (_ != -1) foreach { size =>
      output.write(buffer, 0, size)
      onProgress(size)
    }
  }


  /**
    * Create new file with passed filename. If file exists - add postfix to filename
    *
    * @param filename - filename
    * @param attempt  - attempt
    * @return
    */
  private def createNewFile(filename: String, attempt: Int): File = {
    var newFilename = filename

    if (attempt > 0) {
      val index = filename.lastIndexOf(".")

      newFilename = if (index != -1) {
        filename.substring(0, index) + "(" + attempt + ")" + filename.substring(index)
      } else {
        filename + "(" + attempt + ")"
      }
    }

    val file = new File(newFilename)

    if (file.exists) createNewFile(filename, attempt + 1) else file
  }

  /**
    * Create new file with passed filename. If file exists - add postfix to filename
    *
    * @param filename - filename
    * @return
    */
  private def createNewFile(filename: String): File = createNewFile(filename, 0)

  /**
    * Generate md5 hash of string
    *
    * @param str - incoming string
    * @return
    */
  private def hash(str: String): String = {
    val m = java.security.MessageDigest.getInstance("MD5")
    val b = str.getBytes("UTF-8")
    m.update(b, 0, b.length)
    new java.math.BigInteger(1, m.digest()).toString(16)
  }
}
