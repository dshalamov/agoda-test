package ru.dshalamov.test.util

import java.net.MalformedURLException
import ru.dshalamov.test.net._

/**
  * Created by shalamov on 21.07.17.
  */
class RemoteClient {

    /**
      * Create remote file
      *
      * @param url - url of remote file
      * @return
      */
    def getRemoteFile(url: String): RemoteFile = {
      val _url = URL(url)
      _url.protocol match {
        case p if Array("ftp", "ftps").contains(p) => FtpRemoteFile(_url)
        case p if Array("http", "https").contains(p) => HttpRemoteFile(_url)
        case "sftp" => SftpRemoteFile(_url)
        case p => throw new MalformedURLException(s"Unknown protocol $p")
      }
    }
}
