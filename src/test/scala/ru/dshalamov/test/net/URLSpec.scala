package ru.dshalamov.test.net

import java.net.MalformedURLException

import org.specs2._
import org.specs2.specification._

/**
  * Created by shalamov on 21.07.17.
  */
class URLSpec extends Specification with Tables {
  def is =
    s2"""

  This is a specification to check the class URL

  An URL should
    parse protocol          $e1
    parse host              $e2
    parse path              $e3
    parse port              $e4
    parse username          $e5
    parse password          $e6
    throw MalformedURLException if passed string is not url  $e7

  URL#toBaseURL should create java.net.URL instance  $e8

  The Object URL chould create instance of URL       $e9
    """

  val urls =
    "url" | "protocol" | "host" | "file" | "port" | "user" | "pass" |
      "http://test.com" ! "http" ! "test.com" ! "/" ! None ! None ! None |
      "http://test.com/" ! "http" ! "test.com" ! "/" ! None ! None ! None |
      "ftp://test.com/path.html?test=1" ! "ftp" ! "test.com" ! "/path.html?test=1" ! None ! None ! None |
      "https://subtest.test.com/path.html" ! "https" ! "subtest.test.com" ! "/path.html" ! None ! None ! None |
      "sfpt://u@test.com/path.html" ! "sfpt" ! "test.com" ! "/path.html" ! None ! Some("u") ! None |
      "ftp://u:p@test.com/path.html" ! "ftp" ! "test.com" ! "/path.html" ! None ! Some("u") ! Some("p") |
      "ftps://u:p@test.com:999/" ! "ftps" ! "test.com" ! "/" ! Some(999) ! Some("u") ! Some("p")

  def e1 = urls |> { (url, protocol, host, file, port, user, pass) =>
    new URL(url).protocol must_== protocol
  }

  def e2 = urls |> { (url, protocol, host, file, port, user, pass) =>
    new URL(url).host must_== host
  }

  def e3 = urls |> { (url, protocol, host, file, port, user, pass) =>
    new URL(url).file must_== file
  }

  def e4 = urls |> { (url, protocol, host, file, port, user, pass) =>
    new URL(url).port must_== port
  }

  def e5 = urls |> { (url, protocol, host, file, port, user, pass) =>
    new URL(url).username must_== user
  }

  def e6 = urls |> { (url, protocol, host, file, port, user, pass) =>
    new URL(url).password must_== pass
  }

  def e7 = {
    new URL("invalid string") must throwA[MalformedURLException]
  }

  def e8 = {
    new URL("http://test.com").toBaseURL must beAnInstanceOf[java.net.URL]
  }

  def e9 = {
    URL("http://test.com") must beAnInstanceOf[URL]
  }
}
