package ru.dshalamov.test.util

import java.io.{File, InputStream}
import java.nio.channels.OverlappingFileLockException

import org.specs2._
import org.specs2.mock.Mockito
import ru.dshalamov.test.net.RemoteFile


/**
  * Created by shalamov on 21.07.17.
  */
class RetrieverSpec extends mutable.Specification with Mockito {
  sequential

  trait Context extends mutable.After {
    val directory: File = File.createTempFile("RetrieverSpec", System.nanoTime.toString)
    directory.delete()
    directory.mkdir()

    var data: Array[Int] = Array()

    class TestInputStream extends InputStream {
      private var counter = 0

      override def read(): Int = {
        var res = if (data.length > counter) {
          data(counter)
        } else {
          -1
        }

        counter += 1

        res
      }
    }

    val remoteFile: RemoteFile = mock[RemoteFile]
    remoteFile.getInputStream(any[Int]()) answers { i => new TestInputStream }

    implicit val remoteClient: RemoteClient = mock[RemoteClient]
    remoteClient.getRemoteFile(any[String]()) returns remoteFile

    def after: Any = {
      directory.list foreach { s =>
        val currentFile = new File(directory.getPath, s)
        currentFile.delete()
      }

      directory.delete()
    }
  }

  "Retriever#retrieveFile should" >> {
    "detect filename and download it" in new Context {
      data = Array.fill[Int](2000)(1)
      remoteFile.getFileSize returns 2000
      remoteFile.getFileName returns "e1"

      Retriever.retrieveFile("url1", directory.getAbsolutePath) { (a, b) => }

      new File(directory + "/" + "e1").length() must_== 2000
    }

    "remove tmp file on error" in new Context {
      remoteFile.getFileSize returns 2000
      remoteFile.getFileName returns "e2"

      try {
        Retriever.retrieveFile("url2", directory.getAbsolutePath) { (a, b) => }
      } catch {
        case _: Throwable =>
      }

      new File(directory + "/" + "e2").exists() must beFalse
    }

    "rename file if another already exists" in new Context {
      data = Array.fill[Int](2000)(1)
      remoteFile.getFileSize returns 2000
      remoteFile.getFileName returns "e3"

      val existsFile = new File(directory + "/" + "e3")
      existsFile.createNewFile()

      Retriever.retrieveFile("url3", directory.getAbsolutePath) { (a, b) => }

      new File(directory + "/" + "e3(1)").length() must_== 2000
    }

    "rename file correctly" in new Context {
      data = Array.fill[Int](2000)(1)
      remoteFile.getFileSize returns 2000
      remoteFile.getFileName returns "e3.txt"

      val existsFile = new File(directory + "/" + "e3.txt")
      existsFile.createNewFile()

      Retriever.retrieveFile("url3.1", directory.getAbsolutePath) { (a, b) => }

      new File(directory + "/" + "e3(1).txt").length() must_== 2000
    }

    "download file correctly with concurrent requests" in new Context {

      import scala.concurrent.{Await, Future}
      import scala.concurrent.ExecutionContext.Implicits.global
      import scala.concurrent.duration._

      data = Array.fill[Int](20000)(1)
      remoteFile.getFileSize returns 20000
      remoteFile.getFileName returns "e4"

      val futures = (0 to 3) map { i =>
        Future {
          try {
            Retriever.retrieveFile("url4", directory.getAbsolutePath) { (a, b) => }
          } catch {
            case e: OverlappingFileLockException =>
          }
        }
      }

      futures foreach { f => Await.result(f, 10 second) }

      val file = new File(directory + "/" + "e4")
      file.exists() && file.length() == 20000 must beTrue
    }
  }
}
