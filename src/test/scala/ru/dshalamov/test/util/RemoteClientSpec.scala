package ru.dshalamov.test.util

import java.net.MalformedURLException

import org.specs2._
import ru.dshalamov.test.net.{FtpRemoteFile, HttpRemoteFile, SftpRemoteFile}


/**
  * Created by shalamov on 21.07.17.
  */
class RemoteClientSpec extends mutable.Specification {
  "RemoteClient#getRemoteFile should" >> {
    "create HttpRemoteFile fot http" >> {
      new RemoteClient().getRemoteFile("http://test.com") must beAnInstanceOf[HttpRemoteFile]
    }

    "create HttpRemoteFile fot https" >> {
      new RemoteClient().getRemoteFile("https://test.com") must beAnInstanceOf[HttpRemoteFile]
    }

    "create FtpRemoteFile fot ftp" >> {
      new RemoteClient().getRemoteFile("ftp://test.com") must beAnInstanceOf[FtpRemoteFile]
    }

    "create FtpRemoteFile fot ftps" >> {
      new RemoteClient().getRemoteFile("ftps://test.com") must beAnInstanceOf[FtpRemoteFile]
    }

    "create SftpRemoteFile fot sftp" >> {
      new RemoteClient().getRemoteFile("sftp://test.com") must beAnInstanceOf[SftpRemoteFile]
    }

    "create throw exception for unknown protocol" >> {
      new RemoteClient().getRemoteFile("ttt://test.com") must throwA[MalformedURLException]
    }
  }
}
