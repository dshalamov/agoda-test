name := "home-test"

version := "1.0"

scalaVersion := "2.10.6"

libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10.6"
libraryDependencies += "commons-net"   % "commons-net"    % "3.3"
libraryDependencies += "com.jcraft" % "jsch" % "0.1.54"

libraryDependencies += "org.specs2" %% "specs2-core" % "3.9.1" % "test"
libraryDependencies += "org.specs2" %% "specs2-mock" % "3.9.1" % "test"

scalacOptions in Test += "-Yrangepos"
